"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const babel = require("gulp-babel");
const cssPath = "./css";
const scssRoot = cssPath + "/styles.scss";
const jsPath = "./js";
const jsRoot = jsPath + "/index.js";

// gulp.task('sass', function () {
//   return gulp.src(scssRoot)
//     .pipe(sass.sync().on('error', sass.logError))
//     .pipe(gulp.dest(cssPath));
// });
//
// gulp.task('sass:watch', function () {
//   gulp.watch(cssPath + '/**/*.scss', ['sass']);
// });
//

gulp.task("js", () =>
  gulp
    .src(jsRoot)
    .pipe(
      babel({
        presets: ["@babel/env"]
      })
    )
    .pipe(gulp.dest("dist"))
);
