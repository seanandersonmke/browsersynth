# browsersynth

A synthesizer for the web.

## setup

Run `npm run setup` in your console.

## start local dev server

Run `npm run server` in your console.

## build css files

Run `npm run css:watch` in your console.
