//config.js
var config = {
  audioContext: new (window.AudioContext || window.webkitAudioContext)(),
  isPlaying: false, // Are we currently playing?
  startTime: null, // The start time of the entire sequence.
  current16thNote: null, // What note is currently last scheduled?
  tempo: 120, // tempo (in beats per minute)
  lookahead: 25.0, // How frequently to call scheduling function (in milliseconds)
  scheduleAheadTime: 0.1, // How far ahead to schedule audio (sec). This is calculated from lookahead and overlaps with next interval (in case the timer is late)
  nextNoteTime: 0.0, // when the next note is due.
  noteResolution: 0, // 0 == 16th, 1 == 8th, 2 == quarter note
  noteLength: 0.05, // length of "beep" (in seconds)
  notesInQueue: [], // the notes that have been put into the web audio,
  timerWorker: null, // The Web Worker used to fire timer messages
  notesArray: [] //holds selected notes captured from sequencer form
};

export default config;
