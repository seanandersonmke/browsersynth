import * as noteTable from "./noteTable.js";
import { default as config } from "./config.js";

var noteFreq = noteTable.createNoteTable();
noteTable.createSequencer(noteFreq);

var volumeControl = document.querySelector("input[name='volume']");
var totalNoteCount = document.querySelectorAll(".synth__sequencer__form")
  .length;
var selects = document.querySelectorAll("select");
var nextNotecount = 0;

selects.forEach(function(val) {
  val.addEventListener("change", noteSelectChange);
});
document.querySelector(".synth__playButton").addEventListener("click", play);
document.querySelector(".synth__stopButton").addEventListener("click", stop);
window.addEventListener("load", init);

function noteSelectChange() {
  var forms = document.querySelectorAll(".synth__sequencer__form");
  var count = 0;
  forms.forEach(function(val) {
    config.notesArray[count] = [];
    var key = val[0].value;
    var value = val[1].value;
    val.parentNode.children[0].innerHTML = val[0].value + val[1].value;
    config.notesArray[count][key] = value;
    count++;
  });
}

function nextNote() {
  // Advance current note and time by a 16th note...
  var secondsPerBeat =
    60.0 / document.querySelector("input[name='tempo']").value; // Notice this picks up the CURRENT
  // tempo value to calculate beat length.
  config.nextNoteTime += 0.25 * secondsPerBeat; // Add beat length to last beat time

  config.current16thNote++; // Advance the beat number, wrap to zero

  if (config.current16thNote == totalNoteCount) {
    config.current16thNote = 0;
  }
}

function scheduleNote(beatNumber, time, freq) {
  // push the note on the queue, even if we're not playing.
  config.notesInQueue.push({ note: beatNumber, time: time });
  config.noteResolution = document.querySelector(
    "input[name='noteres']:checked"
  ).value;
  if (config.noteResolution == 1 && beatNumber % 2) return; // we're not playing non-8th 16th notes
  if (config.noteResolution == 2 && beatNumber % 4) return; // we're not playing non-quarter 8th notes

  // create an oscillator
  var osc = config.audioContext.createOscillator();
  var gainNode = config.audioContext.createGain();
  var biquadFilter = config.audioContext.createBiquadFilter();
  // var delay = config.audioContext.createDelay(document.querySelector("input[name='delaytime']").value);
  var delay = config.audioContext.createDelay(5);
  delay.delayTime.setTargetAtTime(
    document.querySelector("input[name='delayamount']").value,
    config.audioContext.currentTime,
    0
  );

  osc
    .connect(biquadFilter)
    .connect(delay)
    .connect(gainNode)
    .connect(config.audioContext.destination);

  osc
    .connect(biquadFilter)
    .connect(gainNode)
    .connect(config.audioContext.destination);

  gainNode.gain.setTargetAtTime(
    volumeControl.value,
    config.audioContext.currentTime,
    0
  );
  osc.frequency.setTargetAtTime(freq, config.audioContext.currentTime, 0);
  osc.type = document.querySelector("input[name='waveform']:checked").value;
  biquadFilter.type = document.querySelector(
    "input[name='filtertype']:checked"
  ).value;
  biquadFilter.frequency.setTargetAtTime(
    document.querySelector("input[name='filterCutoff']").value,
    config.audioContext.currentTime,
    0
  );
  biquadFilter.Q.setTargetAtTime(
    document.querySelector("input[name='filterQ']").value,
    config.audioContext.currentTime,
    0
  );
  osc.start(time);
  osc.stop(time + config.noteLength);
}

function scheduler() {
  while (
    config.nextNoteTime <
    config.audioContext.currentTime + config.scheduleAheadTime
  ) {
    var octave = Object.entries(config.notesArray[nextNotecount])[0][0];
    var note = Object.entries(config.notesArray[nextNotecount])[0][1];
    scheduleNote(
      config.current16thNote,
      config.nextNoteTime,
      noteFreq[octave][note]
    );
    nextNote(nextNotecount);
    nextNotecount++;
    if (nextNotecount === totalNoteCount) {
      nextNotecount = 0;
    }
  }
}

function play() {
  config.isPlaying = !config.isPlaying;

  if (config.isPlaying) {
    // start playing
    config.current16thNote = 0;
    config.nextNoteTime = config.audioContext.currentTime;
    config.timerWorker.postMessage("start");
    return "stop";
  } else {
    config.timerWorker.postMessage("stop");
    return "play";
  }
}

function stop() {
  config.timerWorker.postMessage("stop");
}

function init() {
  config.timerWorker = new Worker("js/metronomeworker.js");

  config.timerWorker.onmessage = function(e) {
    if (e.data == "tick") {
      scheduler();
    } else {
      console.log("message: " + e.data);
    }
  };
  config.timerWorker.postMessage({ interval: config.lookahead });
  noteSelectChange();
}
